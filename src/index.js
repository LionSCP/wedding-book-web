import React from 'react';
import ReactDOM from 'react-dom';

import './assets/sass/master.scss';
import App from "./components/App/App";

import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';

const store = createStore(rootReducer, applyMiddleware(thunk));

const Root = () => {
    return (
        <Provider store={store}>
            <Router>
                <App />
            </Router>
        </Provider>
    )
}

ReactDOM.render(<Root />, document.getElementById('root'));