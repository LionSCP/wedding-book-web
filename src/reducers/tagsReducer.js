import {
GET_ALL_TAGS_START,
GET_ALL_TAGS_SUCCESS,
GET_ALL_TAGS_FAIL
} from '../constants/tagsActionTypes';

const INITIAL_STATE = {
  loading: false,
  error: null,
  tags: []
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_ALL_TAGS_START:
      return {
        ...state,
        loading: true
      }
    case GET_ALL_TAGS_SUCCESS:
      return {
        ...state,
        loading: false,
        tags: action.payload.tags
      }
    case GET_ALL_TAGS_FAIL:
      return {
        ...state,
        error: action.payload.error,
        loading: false
      }
    default:
      return state;
  }
};