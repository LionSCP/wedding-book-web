import { combineReducers } from 'redux';

import searchReducer from './searchReducer';
import picturesReducer from './picturesReducer';
import tagsReducer from './tagsReducer';
import authReducer from './authReducer';
import membersReducer from './membersReducer';

const rootReducer = combineReducers({
  searchReducer,
  picturesReducer,
  tagsReducer,
  membersReducer,
  authReducer
});

export default rootReducer;