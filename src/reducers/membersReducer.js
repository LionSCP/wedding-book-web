import {
  GET_ALL_MEMBERS_FAIL,
  GET_ALL_MEMBERS_START,
  GET_ALL_MEMBERS_SUCCESS
} from '../constants/membersActionTypes';

const INITIAL_STATE = {
  members: [],
  loadingMembers: false,
  error: null
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_ALL_MEMBERS_START:
      return {
        ...state,
        loadingMembers: true
      }
    case GET_ALL_MEMBERS_SUCCESS:
      return {
        ...state,
        members: action.payload.members,
        loadingMembers: false,
      }
    case GET_ALL_MEMBERS_FAIL:
      return { 
        ...state,
        error: action.payload.error,
        loadingMembers: false
      }
    default:
      return state;
  }
}