import {
  LOGIN_START,
  LOGIN_SUCCESS,
  HANDLE_USERNAME_CHANGED,
  HANDLE_PASSWORD_CHANGED,
  CHECK_AUTHENTICATION_START,
  CHECK_AUTHENTICATION_SUCCESS,
  LOGOUT

} from '../constants/authActionTypes';

const INITIAL_STATE = {
  username: '',
  password: '',
  user: null,
  isAuthenticated: false,
  loadingCheck: false,
  loading: false,
  error: null
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOGIN_START:
      return {
        ...state,
        loading: true,
      }
    case LOGIN_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        loading: false,
        user: action.payload.user
      }
    case CHECK_AUTHENTICATION_START:
      return {
        ...state,
      }
    case CHECK_AUTHENTICATION_SUCCESS:
      return {
        ...state,
        isAuthenticated: true
      }
    case HANDLE_USERNAME_CHANGED:
      return {
        ...state,
        username: action.payload.username
      }
    case HANDLE_PASSWORD_CHANGED:
      return {
        ...state,
        password: action.payload.password
      }
    case LOGOUT:
      return {
        username: '',
        password: '',
        user: null,
        isAuthenticated: false,
        loadingCheck: false,
        loading: false,
        error: null
      }
    default: 
      return state
  }
}
