import {
  ADD_KEYWORD,
  ADD_MEMBER,
  REMOVE_KEYWORD,
  REMOVE_MEMBER,
  HANDLE_CHANGE_SEARCH_VALUE,
  CLEAN_SEARCH_VALUE
} from '../constants/searchActionTypes';

const INITIAL_STATE = {
  searchValue: '',
  keywords: [[], []],
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_KEYWORD:
      let keywordsTemp = state.keywords[1];
      keywordsTemp.push(action.payload.keyword);
      return {
        ...state,
        keywords: [[...state.keywords[0]], [...keywordsTemp]],
      }
    case ADD_MEMBER:
      let membersTemp = state.keywords[0];
      membersTemp.push(action.payload.member);
      return {
        ...state,
        keywords: [[...membersTemp], [...state.keywords[1]]],
      }
    case REMOVE_KEYWORD:
      let newKeywords = state.keywords[1];
      newKeywords = newKeywords.filter(item => item !== action.payload.keyword);
      return {
        ...state,
        keywords: [[...state.keywords[0]], [...newKeywords]]
      }
    case REMOVE_MEMBER:
      let newMembers = state.keywords[0];
      newMembers = newMembers.filter(item => item.id !== action.payload.member.id)
      return {
        ...state,
        keywords: [[...newMembers], [...state.keywords[1]]]
      }
    case HANDLE_CHANGE_SEARCH_VALUE:
      return {
        ...state,
        searchValue: action.payload.searchValue
      }
    case CLEAN_SEARCH_VALUE:
      return {
        ...state,
        searchValue: ''
      }

    default:
      return state
  }
}