import {
  GET_PICTURES_START,
  GET_PICTURES_SUCCESS,
  GET_PICTURES_FAIL,
  GET_FILTERED_LIST_START,
  GET_FILTERED_LIST_SUCCESS,
  RESET_RELOAD,
  ON_OPEN_MODAL,
  ON_CLOSE_MODAL
} from '../constants/picturesActionTypes';

const INITIAL_STATE = {
  loading: false,
  error: null,
  pictures: {},
  filteredList: [],
  reload: false,
  openModal: false,
  modalContent: null,
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_PICTURES_START:
      return {
        ...state,
        loading: true,
        error: null
      }

    case GET_PICTURES_SUCCESS:
      return {
        ...state,
        loading: false,
        pictures: action.payload.pictures
      }

    case GET_PICTURES_FAIL:
      return {
        ...state,
        error: action.payload.error,
        loading: false
      }

    case GET_FILTERED_LIST_START:
      return {
        ...state,
        loading: true,
      }

    case GET_FILTERED_LIST_SUCCESS:
      return {
        ...state,
        loading: false,
        filteredList: action.payload.filteredList
      }
    
    case RESET_RELOAD:
      return {
        ...state,
        reload: false
      }
    case ON_OPEN_MODAL:
      return {
        ...state,
        openModal: true,
        modalContent: action.payload.modalContent
      }
    case ON_CLOSE_MODAL:
      return {
        ...state,
        openModal: false,
        modalContent: null
      }
    default:
      return state
  }
};