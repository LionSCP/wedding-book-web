import React from 'react';

const Tag = (props) => {
  const { tag } = props.tag;
  return (
    <div className="tag-wrapper">
      <div className="tag-content">
        <p>{tag}</p>
      </div>
    </div>
  )
}

export default Tag;