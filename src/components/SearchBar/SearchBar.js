import React from 'react';
import { connect } from 'react-redux';
import Autocomplete from '../Commons/Autocomplete/Autocomplete';

import {
  addKeyword,
  addMember,
  getFilteredList,
  handleChangeSearchValue,
  cleanSearchValue
} from '../../actions';

const SearchBar = (props) => {
  const { handleChangeSearchValue, addKeyword, searchValue, refreshPictures, cleanSearchValue } = props;

  const handleClick = () => {
    addKeyword(searchValue);
    refreshPictures();
    cleanSearchValue();
  }

  return (
    <div className="search-bar">
      <div className="search-bar-title">
        <p>Recherchez vos photos <span role="img" aria-label="heart">💚</span></p>
      </div>
{/*       <div className="search-bar-content">
        <input className="search-bar-input" type="text" value={searchValue} onChange={(e) => handleChangeSearchValue(e.target.value)} />
        <button className="search-bar-add-item" onClick={() => handleClick()}><img src="images/add-icon.svg" alt="add" /></button>
      </div> */}
      <Autocomplete 
        refreshPictures={refreshPictures}
      />
    </div>
  )
};

const mapStateToProps = ({ picturesReducer, searchReducer }) => {
  return {
    ...picturesReducer,
    ...searchReducer,
  }
}

const mapDispatchToProps = {
  addKeyword,
  addMember,
  getFilteredList,
  handleChangeSearchValue,
  cleanSearchValue
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);