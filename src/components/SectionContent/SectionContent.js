import React, { Component } from 'react';
import { connect } from 'react-redux';
import ContentCard from '../ContentCard/ContentCard';

export class SectionContent extends Component {

  render() {
    const { user } = this.props;
    return (
      <div className="sectionContent-container">
        <ContentCard
          user={user}
        />
      </div>
    )
  }
}

const mapStateToProps = ({authReducer}) => {
  return {...authReducer};
}

export default connect(mapStateToProps, null)(SectionContent)
