import React, { Component } from 'react';
import Tag from '../Tags/Tags';

import { connect } from 'react-redux';

import {
  getAllTags
} from '../../actions';

export class SectionTags extends Component {

  componentDidMount() {
    const { getAllTags } = this.props;
    getAllTags();
  };

  buildTagsList() {
    const { tags } = this.props;
    const tagsList = tags.map((tag) => {
      let randomKey = Math.floor(Math.random() * Math.floor(99999));
      return (
        <Tag
          key={`${tag.tag}-${randomKey}`}
          tag={tag}
        />
      )
    });
    return tagsList;
  }


  render() {
    const { loading } = this.props;
    if (loading) {
      return (
        <div>
          ...Loading
        </div>
      )
    }
    return (
      <div className="sectionTags-container">
        <div className="sectionTags-tags">
          {this.buildTagsList()}
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({ tagsReducer }) => {
  return {
    ...tagsReducer
  }
}

const mapDispatchToProps = {
  getAllTags
}

export default connect(mapStateToProps, mapDispatchToProps)(SectionTags);
