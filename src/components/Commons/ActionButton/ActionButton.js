import React from 'react';
import { Link } from 'react-router-dom';
import httpService from '../../../services/http/httpService';

const ActionButton = (props) => {

  const handleClick = () => {
    const { id } = props;
    httpService.Pictures.delete(id)
    .then((response) => {
      window.location.reload()
    })
    .catch((err) => {
      console.log(err);
    })
  }

  if (props.type === 'edit') {
    return (
      <Link to={`/admin/pictures/list/${props.type}/${props.id}`} >
        <div className="action-button-wrapper">
          <img src={props.imgpath} alt="edit or delete" />
        </div>
      </Link>
    )
  } else {
    return (
      <div onClick={handleClick} className="action-button-wrapper">
        <img src={props.imgpath} alt="edit or delete" />
      </div>
    )
  }
}

export default ActionButton;