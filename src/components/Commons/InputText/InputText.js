import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  handleChangeUsername,
  handleChangePassword
} from '../../../actions';

export class InputText extends Component {

  state = {
    focused: false
  };

  
  onFocus = (event) => {
    if (event.target.value === "") {
      this.setState({
        focused: !this.state.focused
      });
    }
  }
  
  onBlur = (event) => {
    if (event.target.value === "") {
      this.setState({
        focused: !this.state.focused
      });
    }
  }
  
  render() {
    const { handleChange, name, label } = this.props;

    return (
      <div className="login-form-input">
        <label htmlFor={name} className={this.state.focused ? "active" : null} >{label}</label>
        <input type="text" 
          name={name} 
          onFocus={(event) => this.onFocus(event)} 
          onBlur={(event) => this.onBlur(event)} 
          onChange={(event) => handleChange(event)}/>
      </div>
    )
  }
}

const mapStateToProps = ({ authReducer }) => {
  return { ...authReducer };
}

const mapDispatchToProps = {
  handleChangeUsername,
  handleChangePassword
}

export default connect(mapStateToProps, mapDispatchToProps)(InputText);
