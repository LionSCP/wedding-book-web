import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import {
  authLogout
} from '../../../actions';

const LogoutButton = (props) => {
  
  const handleClick = () => {
    const { authLogout, history } = props;
    authLogout(history);
  }

  if (props.isAuthenticated) {
    return (
      <button className="logout-btn" onClick={handleClick}>Déconnexion</button>
    )
  } else return null
}

const mapStateToProps = ({ authReducer }) => {
  return {
    ...authReducer
  }
};

const mapDispatchToProps = {
  authLogout
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LogoutButton));