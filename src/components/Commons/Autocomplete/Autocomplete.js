import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  handleChangeSearchValue,
  addMember,
  addKeyword,
  cleanSearchValue
} from '../../../actions';

export class Autocomplete extends Component {

  state = {
    activeSuggestion: 0,
    filteredSuggestion: [],
    showSuggestions: false,
  };

  componentDidMount() {
  }

  filterSuggestions = (value) => {
    const { members, tags } = this.props;

    let filteredMembersPhaseOne = members.filter(suggestion => {
      return suggestion.first_name.toLowerCase().includes(value);
    })
    let filteredMembersPhaseTwo = members.filter(suggestion => {
      return suggestion.last_name.toLowerCase().includes(value);
    });

    let filteredMembersSuggestions = []
    filteredMembersSuggestions.push(...filteredMembersPhaseOne, ...filteredMembersPhaseTwo);
    filteredMembersSuggestions = [...new Set(filteredMembersSuggestions)];

    let filteredTagsSuggestions = tags.filter(suggestion => {
      return suggestion.tag.includes(value);
    })
    filteredTagsSuggestions = filteredTagsSuggestions.map(tag => tag.tag);

    this.setState({
      filteredSuggestions: [
        filteredMembersSuggestions,
        filteredTagsSuggestions
      ],
      showSuggestions: true,
    })
  }

  resetState = () => {
    const { cleanSearchValue } = this.props;
    this.setState({
      filteredSuggestions: [],
      showSuggestions: false
    });
    cleanSearchValue();

  }

  handleClick = (item) => {
    const { addMember, addKeyword, refreshPictures } = this.props;

    item.id ? addMember(item) : addKeyword(item);
    refreshPictures();
    this.resetState();
  }

  buildMembersList() {
    const { filteredSuggestions } = this.state;

    const membersListSuggestions = filteredSuggestions[0].map(suggestion => {
      return (
        <li
          key={suggestion.id}
          className="suggestions-block-item"
          onClick={() => this.handleClick(suggestion)}
        >
          {`${suggestion.first_name} ${suggestion.last_name}`}
        </li>
      )
    })

    return membersListSuggestions;
  }

  buildTagsList() {
    const { filteredSuggestions } = this.state;

    const tagsListSuggestions = filteredSuggestions[1].map(suggestion => {
      return (
        <li
          key={`${suggestion}-${new Date().getTime()}`}
          className="suggestions-block-item"
          onClick={() => this.handleClick(suggestion)}
        >
          {`${suggestion}`}
        </li>
      )
    })

    return tagsListSuggestions;
  }

  buildSuggestionsList() {
    const { searchValue } = this.props;
    const { filteredSuggestions, showSuggestions } = this.state;

    if (searchValue && showSuggestions) {
      if (filteredSuggestions[0].length || filteredSuggestions[1].length) {
        return (
          <div className="suggestions">
            <p className="suggestions-title">Invités</p>
            <ul className="suggestions-block">
              {this.buildMembersList()}
            </ul>
            <p className="suggestions-title">Tags</p>
            <ul className="suggestions-block">
              {this.buildTagsList()}
            </ul>
          </div>
        )
      } else {
        return (
          <div className="suggestions">
            Pas de résultats !
          </div>
        )
      }
    } else return null
  }

  render() {
    const { handleChangeSearchValue, searchValue } = this.props;

    return (
      <div className="search-bar-content">
        <input
          className="search-bar-input"
          type="text"
          value={searchValue}
          onChange={(e) => {
            handleChangeSearchValue(e.target.value);
            this.filterSuggestions(e.target.value);

          }} />
        {this.buildSuggestionsList()}
        {/* <button className="search-bar-add-item" onClick={() => handleClick()}><img src="images/add-icon.svg" alt="add" /></button> */}
      </div>
    )
  }
}

const mapStateToProps = ({ membersReducer, tagsReducer, searchReducer }) => {
  return {
    ...membersReducer,
    ...tagsReducer,
    ...searchReducer
  }
}

const mapDisptachToProps = {
  handleChangeSearchValue,
  addMember,
  addKeyword,
  cleanSearchValue
};

export default connect(mapStateToProps, mapDisptachToProps)(Autocomplete);

