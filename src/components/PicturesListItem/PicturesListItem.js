import React from 'react';
import ActionButton from '../Commons/ActionButton/ActionButton';

const PicturesListItem = (props) => {

  const buildMembersList = () => {
    const { members } = props;

    const membersList = members.map((member) => {
      return (
        <li key={member.id}>{`${member.first_name} ${member.last_name}`}</li>
      )
    })

    return membersList
  }

  const buildTagsList = () => {
    const { tags } = props;
    let tagsList = '';

    for (let i = 0; i < tags.length; i++) {
      tagsList += `${tags[i]}, `;
    }

    return (
      <p>
        {tagsList}
      </p>
    )
  }
  return (
    <div
      className="pictures-list-item-wrapper"

    >
      <div
        className="pictures-list-item-content"
        onClick={() => { props.customClickEvent(props.id) }}
      >
        <div
          className="pictures-list-item-avatar"
          style={{
            backgroundImage: `url(${props.path})`,
            backgroundPosition: 'center',
            backgroundSize: 'contain'
          }}>
        </div>
        <div className="pictures-list-item-description">
          <div className="pictures-list-item-description-members">
            <p className="pictures-list-item-description-title">Invités</p>
            <ul>
              {props.members && buildMembersList()}
            </ul>
          </div>
          <div className="pictures-list-item-description-tags">
            <p className="pictures-list-item-description-title">Tags</p>
            {props.tags && buildTagsList()}
          </div>
        </div>
      </div>
      <div className="pictures-list-item-action">
        <ActionButton
          id={props.id}
          imgpath={'/images/edit-button.svg'}
          type='edit'
        />
        <ActionButton
          id={props.id}
          imgpath={'/images/delete-button.svg'}
          type='delete'
        />
      </div>
    </div>
  )
}

export default PicturesListItem;