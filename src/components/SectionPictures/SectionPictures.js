import React, { Component } from 'react';
import Cards from '../Cards/Cards';
import SearchBar from '../SearchBar/SearchBar';
import KeywordList from '../KeywordsList/KeywordList';
import Modal from 'react-responsive-modal';
import httpService from '../../services/http/httpService';
import download from 'downloadjs';


import { connect } from 'react-redux';

import {
  addKeyword,
  addMember,
  removeKeyword,
  removeMember,
  getAllPictures,
  getFilteredList,
  handleChangeSearchValue,
  getAllMembers,
  onCloseModal,
  onOpenModal
} from '../../actions';

export class SectionPictures extends Component {

  componentDidMount() {
    this.props.getAllPictures();
    this.props.getAllMembers();
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  refreshPictures() {
    const { pictures, keywords, getFilteredList } = this.props;
    getFilteredList(pictures, keywords);
  }

  buildPicturesList() {
    const { pictures, filteredList, onOpenModal } = this.props;
    const sourceList = filteredList.length === 0 ? pictures : filteredList;
    const picturesList = Object
      .keys(sourceList)
      .map((picture, index) => {
        return (
          <Cards
            key={sourceList[picture]._id}
            path={sourceList[picture].path}
            index={index}
            onOpenModal={onOpenModal}
            picture={sourceList[picture]}
          />
        )
      });
    return picturesList;
  }

  buildKeywordsList() {
    const { keywords, removeKeyword, removeMember } = this.props;
    if (keywords[0].length > 0 || keywords[1].length > 0) {
      return (
        <KeywordList
          refreshPictures={this.refreshPictures.bind(this)}
          removeKeyword={removeKeyword}
          removeMember={removeMember}
          keywords={keywords}
        />
      );
    } else return null
  }

  download() {
    const { modalContent } = this.props;
    const arraySplit = modalContent.path.split('/');
    const name = arraySplit[arraySplit.length - 1];
    httpService.Pictures.download(modalContent._id)
    .then((response) => {
      download(response.data, name, "image/jpg");
    })
    .catch((error) => {
      console.log(error);
    })
  }

  render() {
    const { loading, openModal, onCloseModal, modalContent } = this.props;
    let style = null;

    if (loading) {
      return (
        <div>
          ...Loading
        </div>
      )
    }

    if (modalContent) {
      style = {
        backgroundImage: `url(${modalContent.path})`,
        backgroundPosition: 'center',
      }
    }
    return (
      <div className="pictures-container">
        <div className="pictures-search">
          <SearchBar
            refreshPictures={this.refreshPictures.bind(this)}
          />
        </div>
        <div className="pictures-keywords">
          {this.buildKeywordsList()}
        </div>
        <div className="pictures-portfolio">
          {this.buildPicturesList()}
        </div>
        <Modal 
          open={openModal} 
          onClose={onCloseModal} 
          center
          closeIconSize={40}
          closeIconId="modal-close-icon"
        >
          <div className="pictures-modal">
            <div
              className="pictures-modal-file"
              style={style}
            />
            <button onClick={this.download.bind(this)}>Télécharger</button>
          </div>
        </Modal>
      </div>
    )
  }
}

const mapStateToProps = ({ picturesReducer, searchReducer }) => {
  return {
    ...picturesReducer,
    ...searchReducer
  }
}

const mapDispatchToProps = {
  addKeyword,
  addMember,
  removeKeyword,
  removeMember,
  getAllPictures,
  getFilteredList,
  handleChangeSearchValue,
  getAllMembers,
  onCloseModal,
  onOpenModal
}

export default connect(mapStateToProps, mapDispatchToProps)(SectionPictures);
