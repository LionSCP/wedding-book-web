import React from 'react';
import { withRouter, Link } from 'react-router-dom';

const AdminButton = (props) => {
  return (
    <Link to="/admin">
      <div className="admin-button">
        <img src="images/settings.svg" alt="settings" />
      </div>
    </Link>
  )
}

export default withRouter(AdminButton);