import React from 'react';

const Header = () => {
  
  return (
      <div className="header-container">
        <div className="icon-container">
          <h1>Notre wedding book</h1>
        </div>
      </div>
  )
}

export default Header
