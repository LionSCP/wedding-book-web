import React from 'react';
import Keyword from '../Keyword/Keyword';

const KeywordList = (props) => {
  const { keywords, removeKeyword,removeMember, refreshPictures } = props;

  const keywordsList = keywords.map((keywordGroup, index) => {
    return keywordGroup.map(keyword => {
      const key = keyword.id ? keyword.id : `${keyword}-${new Date().getTime()}`;
      return (
        <Keyword 
          value={keyword}
          removeKeyword={removeKeyword}
          removeMember={removeMember}
          key={key}
          refreshPictures={refreshPictures}
          keywords={keywords}
        />
      )
    })
  })
  return (
    <div className="pictures-keywords-list">
      {keywordsList}
    </div>
  )
}

export default KeywordList;