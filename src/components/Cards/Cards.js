import React from 'react';

const CLASSNAME = ["square", "long", "mini", "short", "short", "big", "mini", "mini"];

const Cards = (props) => {

    const style = {
        backgroundImage: `url(${props.path})`,
        //backgroundPosition: 'center'
    }
    const className = `cards ${CLASSNAME[props.index % CLASSNAME.length]}`;
    
    return (
        <div
            className={className}
            style={style}
            onClick={() => props.onOpenModal(props.picture)}
        >
        </div>
    )
}

export default Cards;
