import React, { Component } from 'react'
import { Route, Switch, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import Header from '../Header/Header';
import Footer from '../Footer/Footer';

import Home from '../../views/Home/Home';
import Admin from '../../views/Admin/Admin';
import Login from '../../views/Login/Login';
import Pictures from '../../views/Pictures/Pictures';
import Picture from '../../views/Picture/Picture';
import UploadPictures from '../../views/UploadPictures/UploadPictures';

import {
  authCheckAuthentication
} from '../../actions';

export class App extends Component {

  componentDidMount() {
    const { authCheckAuthentication, history } = this.props;

    authCheckAuthentication(history);
  }

  render() {

    const { isAuthenticated } = this.props;

    return (
      <div>
        <Header />
        <Switch>
          <Route exact path="/"
            render={() => {
              if (isAuthenticated) {
                return (<Home />)
              } else {
                return (<Login {...this.props} />)
              }
            }} />
          <Route exact path="/admin"
            render={() => {
              if (isAuthenticated) {
                return (<Admin />)
              } else {
                return (
                  <Login {...this.props} />
                )
              }
            }} />
          <Route
            exact path="/admin/pictures/list"
            render={() => {
              if (isAuthenticated) {
                return (<Pictures />)
              } else {
                return (
                  <Login {...this.props} />
                )
              }
            }} />
          <Route
            exact path="/admin/pictures/list/edit/:id"
            render={() => {
              if (isAuthenticated) {
                return (<Picture />)
              } else {
                return (
                  <Login {...this.props} />
                )
              }
            }} />
          <Route
            exact path="/admin/pictures/upload"
            render={() => {
              if (isAuthenticated) {
                return (<UploadPictures />)
              } else {
                return (
                  <Login {...this.props} />
                )
              }
            }} />
          <Route
            exact path="/login"
            component={Login}
            {...this.props}
          />
        </Switch>
        <Footer />
      </div>
    )
  }
}

const mapStateToProps = ({ authReducer }) => {
  return {
    ...authReducer,
  }
}

const mapDispatchToProps = {
  authCheckAuthentication
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));

