import React from 'react';

import SearchBar from '../SearchBar/SearchBar';

const Head = () => {
  return (
    <div className="head-container">
      <SearchBar/>
    </div>
  )
}

export default Head;