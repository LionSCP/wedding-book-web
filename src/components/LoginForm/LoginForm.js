import React, { Component } from 'react';
import { connect } from 'react-redux';
import InputText from '../../components/Commons/InputText/InputText';

import {
  authLogin,
  handleChangeUsername,
  handleChangePassword
} from '../../actions';

export class LoginForm extends Component {

  constructor(props) {
    super(props);
    this.handleChangeFormUsername = this.handleChangeFormUsername.bind(this);
    this.handleChangeFormPassword = this.handleChangeFormPassword.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangeFormUsername(event) {
    const { handleChangeUsername } = this.props;
    event.preventDefault();
    const value = event.target.value;
    handleChangeUsername(value);
  }
  
  handleChangeFormPassword(event) {
    const { handleChangePassword } = this.props;
    event.preventDefault();
    const value = event.target.value;
    handleChangePassword(value);
  }
  
  handleSubmit(event) {
    const { authLogin, username, password, history } = this.props;
    event.preventDefault();
    const body = { username, password };
    authLogin(body, history);
  }
  
  render() {
    const { username, password } = this.props;
    return (
      <form name="form" className="login-form" onSubmit={this.handleSubmit}>
        <InputText name="username" value={username} handleChange={this.handleChangeFormUsername} label="Insérez votre identifiant" />
        <InputText name="password" value={password} handleChange={this.handleChangeFormPassword} label="Insérez votre mot de passe" />
        <button className="btn">Se connecter</button>
      </form>
    )
  }
}

const mapStateToProps = ({ authReducer }) => {
  return { ...authReducer };
}

const mapDispatchToProps = {
  authLogin,
  handleChangeUsername,
  handleChangePassword
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
