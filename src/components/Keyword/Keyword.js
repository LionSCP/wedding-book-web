import React from 'react';

const Keyword = (props) => {

  const { value, removeKeyword, removeMember, refreshPictures } = props;
  const handleClick = async () => {
    value.id ? await removeMember(value) : await removeKeyword(value)
    refreshPictures();
  }
  let item;
  value.id ? item = `${value.first_name} ${value.last_name}` : item = value;
  return (
    <div className="pictures-keywords-item">
      <p>{item}</p>
      <button className="keyword-close" onClick={() => handleClick()}><img src="images/close-icon.svg" alt="close"/></button>
    </div>
  )
}

export default Keyword;