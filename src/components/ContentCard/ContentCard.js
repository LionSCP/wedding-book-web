import React from 'react';
import utils from '../../services/utils/utils';
import LogoutButton from '../Commons/LogoutButton/LogoutButton';

const ContentCard = (props) => {
  const { user } = props;

  return (
    <div className="content-card-wrapper">
      <div className="content-card-avatar">
        <img src={user && `${user.avatar}`} alt="man" />
      </div>
      <div className="content-card-content">
        <div className="content-card-name">
          <p>{user && `${user.first_name} ${user.last_name}`}</p>
        </div>
        <div className="content-card-relation">
          {user && `${utils.toUppercaseFirstLetter(user.relation_label)} ${user.relationwith_label}`}
        </div>
        <LogoutButton />
      </div>
    </div>
  )
};

export default ContentCard;