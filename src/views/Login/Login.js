import React from 'react';
import LoginForm from '../../components/LoginForm/LoginForm';



const Login = (props) => {
  return (
    <div className="login-container">
      <div className="login-title-border-wrap">
        <div className="login-title-wrapper">
          <p className="login-title">Connectez-vous avec les identifiants reçus lors du mariage et profitez des photos de ce magnifique évènement</p>
        </div>
      </div>
      <LoginForm {...props}/>
      <div className="login-country">
        <div className="login-box-wrapper">
          <img src="images/portugal.svg" alt="portugal"/>
        </div>

        <div className="login-box-wrapper">
        <img src="images/france.svg" alt="france"/>
        </div>
      </div>
    </div>
  )
}

export default Login;