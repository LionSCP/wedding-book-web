import React from 'react';
import { withRouter, Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const Admin = (props) => {
  const { user } = props;
  if (user && user.role === 'SUPER_ADMIN') {
    return (
      <div className="main-admin-container">
        <div className="main-admin-head">
          <h1>
            Gérer l'application
            </h1>
        </div>
        <div className="main-admin-topic">
          <div className="main-admin-card">
            <Link to="/admin/pictures/list">
              <div className="card-icon">
                <img src="images/pictures.svg" alt="pictures" />
              </div>
            </Link>
          </div>
          <div className="main-admin-card">
            <Link to="/admin/pictures/upload">
              <div className="card-icon">
                <img src="images/upload-files-admin-ui.svg" alt="pictures" />
              </div>
            </Link>
          </div>
          <div className="main-admin-card">
            <Link to="/admin/users/list">
              <div className="card-icon">
                <img src="images/users.svg" alt="pictures" />
              </div>
            </Link>
          </div>
        </div>
      </div>
    )
  } else {
    return (
      <Redirect to="/"/>
    )
  }
} 


const mapStateToProps = ({authReducer}) => {
  return {
    user: authReducer.user
  }
}

export default withRouter(connect(mapStateToProps, null)(Admin));