import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PicturesListItem from '../../components/PicturesListItem/PicturesListItem';

import {
  getAllPictures
} from '../../actions';

export class Pictures extends Component {

  async componentDidMount() {
    const { pictures, getAllPictures } = this.props;
    if (Object.entries(pictures).length === 0 && pictures.constructor === Object) {
      getAllPictures();
    }
  }

  redirectPage(id) {
    this.props.history.push(`list/edit/${id}`);
  }

  buildPictures() {
    const { pictures } = this.props;
    const picturesList = Object
      .keys(pictures)
      .map((picture) => {
        return (
          <PicturesListItem
            key={pictures[picture]._id}
            path={pictures[picture].path}
            members={pictures[picture].members}
            tags={pictures[picture].tags}
            id={pictures[picture]._id}
            customClickEvent={this.redirectPage.bind(this)}
          />
        )
      })
    return picturesList;
  }

  render() {
    const { loading } = this.props;
    if (loading) {
      return (
        <div>
          ...Loading
        </div>
      )
    }
    return (
      <div className="admin-pictures-container">
        {this.buildPictures()}
      </div>
    )

  }
}
const mapStateToProps = ({ picturesReducer }) => {
  return {
    pictures: picturesReducer.pictures
  }
};

const mapDispatchToProps = {
  getAllPictures
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Pictures));
