import React, { Component } from 'react'
import Dropzone from 'react-dropzone';
import httpService from '../../services/http/httpService';

export class UploadPictures extends Component {

  state = {
    files: []
  }

  onDrop = (acceptedFiles) => {
    console.log("Accepted files:", acceptedFiles);
    this.setState({
      files: acceptedFiles
    })
  }

  handleSubmit = () => {
    const formData = new FormData();
    const { files } = this.state;
    for (let file of files) {
      formData.append('pictures', file);
    }
    httpService.Pictures.upload(formData);
  }

  render() {
    const maxSize = 5242880;

    return (
      <div className="upload-main-container">
        <div className="upload-header">
          <h2>Chargez les photos ici</h2>
        </div>
        <div className="upload-dropzone">
          <div className="upload-dropzone-component">
            <Dropzone
              onDrop={this.onDrop.bind(this)}
              accept="image/png, image/jpeg"
              minSize={0}
              maxSize={maxSize}
              multiple
            >
              {({ getRootProps, getInputProps, isDragActive, isDragReject, rejectedFiles, acceptedFiles }) => {
                const isFileTooLarge = rejectedFiles.length > 0 && rejectedFiles[0].size > maxSize;
                return (
                  <div {...getRootProps()}>
                    <input type="file" name="pictures" multiple {...getInputProps()}/>
                    {!isDragActive && (
                      <div className="upload-wrapper">
                        <div className="upload-icon">
                          <img src="/images/upload-files.svg" alt="upload" />
                        </div>
                        <p>Uploader les photos</p>
                      </div>
                    )}
                    {isDragActive && !isDragReject && "Drop it like it's hot!"}
                    {isDragReject && "File type not accepted, sorry!"}
                    {isFileTooLarge && (
                      <div>
                        <p>File is too large.</p>
                      </div>
                    )}
                    <ul className="upload-accepted-files">
                      {acceptedFiles.length > 0 && acceptedFiles.map(acceptedFile => (
                        <li
                          className="upload-accepted-item"
                          key={acceptedFile.name + "2019"}
                        >
                          {acceptedFile.name}
                        </li>
                      ))}
                    </ul>
                  </div>
                )
              }}

            </Dropzone>
          </div>

        </div>
        <div className="upload-submit">
          <button onClick={this.handleSubmit.bind(this)} type="submit">
            Upload
          </button>
        </div>
        <div className="upload-footer">

        </div>
      </div>
    )
  }
}

export default UploadPictures
