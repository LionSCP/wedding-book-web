import React, { Component } from 'react'
import { withRouter } from 'react-router-dom';
import Select from 'react-select';
import Creatable from 'react-select/creatable'
import httpService from '../../services/http/httpService';

const API_URL = process.env.NODE_ENV === "production" ? "https://sergioandguidinha.com" : "http://localhost:5000";

export class Picture extends Component {

  state = {
    currentPicture: null,
    currentMembers: null,
    currentTags: null,
    selectedMembers: null,
    selectedTags: null,
    membersOptions: null,
    tagsOptions: null,
    isMemberRemoved: false,
    isMemberAdded: false,
    isTagRemoved: false,
    isTagAdded: false
  };

  async componentDidMount() {
    await this.getCurrentPicture();
    await this.getAllGuests();
    await this.getAllTags();
  }

  async getAllGuests() {
    const response = await httpService.Guest.getAllGuests();
    const guests = response.data.data;

    const options = guests.map((guest) => {
      return {
        value: guest.id,
        label: `${guest.first_name} ${guest.last_name}`
      }
    });

    this.setState({ membersOptions: options });
  }

  async getAllTags() {
    const response = await httpService.Tags.getAllTags();
    const tags = response.data.data;

    const options = tags.map((tag) => {
      return {
        value: tag.tag,
        label: tag.tag
      }
    });

    this.setState({
      tagsOptions: options,
      tags
    })
  }

  async getCurrentPicture() {
    const { match: { params } } = this.props;

    const response = await httpService.Pictures.getById(params.id);
    const picture = response.data.data;
    let clearPath = picture.path.split(String.fromCharCode(92)).join("/");
    picture.path = `${API_URL}/${clearPath}`;
    this.setState({
      currentPicture: picture,
      currentMembers: picture.members,
      currentTags: picture.tags
    });
  }

  deleteGuestFromList(memberId) {
    const { currentMembers } = this.state;

    const newMembersList = currentMembers.filter((member) => {
      return member.id !== memberId;
    });

    this.setState({
      currentMembers: newMembersList,
      isMemberRemoved: true,
    });
  };

  deleteTagFromList(deletedTag) {
    const { currentTags } = this.state;

    const newTagsList = currentTags.filter((tag) => {
      return deletedTag !== tag
    });

    this.setState({
      currentTags: newTagsList,
      isTagRemoved: true,
    });
  };

  buildMembersList() {
    const { currentMembers } = this.state;

    const membersList = currentMembers.map((member) => {
      return (
        <li key={member.id}>
          <div className='description-list-item'>
            <p>{`${member.first_name} ${member.last_name}`}</p>
            <img src="/images/clear-button.svg" alt="clear" onClick={() => this.deleteGuestFromList(member.id)} />
          </div>
        </li>
      )
    });
    return membersList;
  }

  buildTagsList() {
    const { currentTags } = this.state;

    const tagsList = currentTags.map((tag) => {
      return (
        <li key={`${tag}-${new Date().getTime()}`}>
          <div className='description-list-item'>
            <p>{tag}</p>
            <img src="/images/clear-button.svg" alt="clear" onClick={() => this.deleteTagFromList(tag)} />
          </div>
        </li>
      )
    });
    return tagsList;
  }

  buildSubmitButton() {
    const { isMemberAdded, isMemberRemoved, isTagAdded, isTagRemoved } = this.state;

    if (isMemberAdded || isMemberRemoved || isTagAdded || isTagRemoved) {
      return (
        <div
          onClick={this.handleSubmit.bind(this)}
          className="picture-submit-btn modified"
        >
          <p>Modifier</p>
        </div>
      )
    } else {
      return (
        <div className="picture-submit-btn">
          <p>Modifier</p>
        </div>
      )
    }
  }

  async handleSubmit() {
    const {
      isMemberAdded,
      isMemberRemoved,
      isTagAdded,
      isTagRemoved,
      selectedMembers,
      selectedTags,
      currentMembers,
      currentTags
    } = this.state;
    const { match: { params } } = this.props;

    const requests = [];


    let newMembers = [];
    let newTags = [];

    newMembers = currentMembers.map(member => {
      return member.id
    })

    if (isMemberAdded) {
      for (let guest of selectedMembers) {
        newMembers.push(guest.value);
      }
    }

    if (isTagAdded) {
      for (let tag of selectedTags) {
        newTags.push(tag.value)
      }
    }

    newTags = [...newTags, ...currentTags];
    newTags = [...new Set(newTags)];
    newMembers = [...new Set(newMembers)];

    if (isMemberAdded || isMemberRemoved) {
      requests.push(httpService.Pictures.update(params.id, { members: newMembers }));
    }

    if (isTagAdded || isTagRemoved) {
      requests.push(httpService.Pictures.update(params.id, { tags: newTags }));
    }

    Promise.all(requests).then(() => {
      window.location.reload();
    })


  }


  handleChangeGuests(selectedOption) {
    this.setState({
      selectedMembers: selectedOption,
      isMemberAdded: true,
    });

    if (!selectedOption || selectedOption.length === 0) {
      this.setState({
        isMemberAdded: false,
      })
    }
  }

  handleChangeTags(selectedOption) {
    this.setState({
      selectedTags: selectedOption,
      isTagAdded: true
    });

    if (!selectedOption || selectedOption.length === 0) {
      this.setState({
        isTagAdded: false
      })
    }
  }

  render() {

    const customStyles = {
      option: (provided, state) => ({
        ...provided,
        color: '#4DAF7C',
      }),
      container: () => ({
        marginTop: '1rem',
        width: '100%',
      }),
      control: (provided, state) => ({
        ...provided,
        borderColor: '#4DAF7C',
        boxShadow: state.isFocused && '0 0 0 1px #4DAF7C',

        '&:hover': {
          boxShadow: '0 0 0 1px #4DAF7C',
        }
      }),
      menu: () => ({
        width: '20rem',
        position: 'absolute',
        backgroundColor: 'white',
        zIndex: '10'
      }),
      dropdownIndicator: () => ({
        color: '#4DAF7C',
        padding: '0 1rem 0 1rem'
      })
    }

    if (this.state.currentPicture) {
      const { currentPicture, selectedOption, membersOptions, tagsOptions } = this.state;
      return (
        <div className="picture-container">
          <div className="picture-content">
            <div className="card-wrapper">
              <div className="card-header">
                <p>Photo n° {currentPicture._id}</p>
              </div>
              <div
                className="card-content-picture"
                style={{
                  backgroundImage: `url(${currentPicture.path})`,
                  backgroundPosition: 'center',
                  backgroundSize: 'contain'
                }}>
              </div>
            </div>
            <div className="card-wrapper">
              <div className="card-header">
                <p>Liste des invités</p>
              </div>
              <div className="card-content-description">
                <ul className="description-list">
                  {this.buildMembersList()}
                  <Select
                    value={selectedOption}
                    onChange={this.handleChangeGuests.bind(this)}
                    options={membersOptions}
                    styles={customStyles}
                    isMulti="true"
                    placeholder="Ajouter un invité..."
                  />
                </ul>
              </div>
            </div>
            <div className="card-wrapper">
              <div className="card-header">
                <p>Liste des tags</p>
              </div>
              <div className="card-content-description">
                <ul className="description-list">
                  {this.buildTagsList()}
                  <Creatable
                    value={selectedOption}
                    onChange={this.handleChangeTags.bind(this)}
                    options={tagsOptions}
                    styles={customStyles}
                    isMulti="true"
                    name="tags-select"
                    placeholder="Ajouter un tag..."
                  />
                </ul>
              </div>
            </div>
          </div>
          <div className="picture-submit">
            {this.buildSubmitButton()}
          </div>
        </div>
      )
    } else {
      return (
        <div>
          ...Loading
        </div>
      );
    }

  }
}

export default withRouter(Picture);
