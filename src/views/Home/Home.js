import React from 'react';
import { connect } from 'react-redux';

import SectionPictures from '../../components/SectionPictures/SectionPictures';
import SectionTags from '../../components/SectionTags/SectionTags';
import Bottom from '../../components/Bottom/Bottom';
import SectionContent from '../../components/SectionContent/SectionContent';
import AdminButton from '../../components/AdminButton/AdminButton';

const Home = (props) => {
  const renderAdminButton = () => {
  
    if (props.user && props.user.role === "SUPER_ADMIN") {
      return (
        <AdminButton />
      )
    }
  }

  return (
      <div className="main-container">
        {renderAdminButton()}
        <SectionContent></SectionContent>
        <SectionPictures></SectionPictures>
        <SectionTags></SectionTags>
        <Bottom></Bottom>
      </div>
  )
};

const mapStateToProps = ({authReducer}) => {
  return {
    ...authReducer
  }
}

export default connect(mapStateToProps, null)(Home)
