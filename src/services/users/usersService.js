const decodeRelationOfUser = (user) => {
  switch (user.relation) {
    case 0:
      if (user.gender === "male") return "marié";
      else if (user.gender === "female") return "mariée";
      break;
    case 1:
      if (user.gender === "male") return "père";
      else if (user.gender === "female") return "mère";
      break;
    case 2:
      if (user.gender === "male") return "frère";
      else if (user.gender === "female") return "soeur";
      break;
    case 3:
      if (user.gender === "male") return "cousin";
      else if (user.gender === "female") return "cousine";
      break;
    case 4:
      if (user.gender === "male") return "oncle";
      else if (user.gender === "female") return "tante";
      break;
    case 5:
      if (user.gender === "male") return "grand-père";
      else if (user.gender === "female") return "grand-mère";
      break;
    case 6:
      if (user.gender === "male") return "ami";
      else if (user.gender === "female") return "amie";
      break;
    case 7:
      if (user.gender === "male") return "parrain";
      else if (user.gender === "female") return "marraine";
      break;
    case 8:
      if (user.gender === "male") return "demoiselle d'honneur";
      else if (user.gender === "female") return "homme d'honneur";
      break;
    default:
      break;
  }
}

const decodeRelationWith = (user) => {
  switch (user.relation_with) {
    case 0:
      return "de Guidinha"; 
    case 1:
      return "de Sergio"
    case 2:
      return "des mariés"
    default:
      break;
  }
}

const addAvatar = (user) => {
  switch (user.gender) {
    case "male":
      return "images/avatar-man.svg";
    case "female":
      return "images/avatar-woman.svg";
    default:
      return "images/avatar-man/svg";
  }
}

const formatUser = (user) => {
  const relation_label = decodeRelationOfUser(user);
  const relationwith_label = decodeRelationWith(user);
  const avatar = addAvatar(user);
  return {
    ...user,
    relation_label,
    relationwith_label,
    avatar
  }
};

export default {
  decodeRelationOfUser,
  decodeRelationWith,
  formatUser
};