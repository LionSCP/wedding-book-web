import httpService from '../http/httpService';
import decode from 'jwt-decode';
import axios from 'axios';
import usersService from '../users/usersService';

import * as types from '../../constants/authActionTypes';

const API_URL = process.env.NODE_ENV === "production" ? "https://sergioandguidinha.com/" : "http://localhost:5000/";

export default class Auth {
  token;
  expirationDate;
  userProfile;

  constructor() {
    this.login = this.login.bind(this);
    this.setSession = this.setSession.bind(this);
    this.getToken = this.getToken.bind(this);
    this.getUserProfile = this.getUserProfile.bind(this);
    this.getExpirationDate = this.getExpirationDate.bind(this);
    this.isAuthenticated = this.isAuthenticated.bind(this);
    this.renewSession = this.renewSession.bind(this);
  }

  getToken() {
    return this.token;
  }

  getExpirationDate() {
    return this.expirationDate;
  }

  getUserProfile() {
    return this.userProfile;
  }

  login(body, history, dispatch) {
    httpService.Auth.login(body)
      .then((res) => {
        if (!res) {
          throw new Error("No response from server");
        }
        this.setSession(res.data.token, res.data.user, history, dispatch);
      })
      .catch(err => {
        console.log(err);
      })
  }

  logout(history) {
    console.log('...logout !')

    this.token = null;
    this.userProfile = null;

    localStorage.removeItem('isLoggedIn');
    localStorage.removeItem('token');

    history.replace('/login');
  }

  setSession(token, user, history, dispatch) {
    let alreadyAuthenticated = false;

    if (localStorage.getItem("isLoggedIn") === 'true' && localStorage.getItem('token') && this.isAuthenticated()) {
      alreadyAuthenticated = true;
    }

    this.token = token;
    this.userProfile = usersService.formatUser(user);

    localStorage.setItem('isLoggedIn', 'true');
    localStorage.setItem('token', token);
    dispatch({
      type: types.LOGIN_SUCCESS,
      payload: { user: this.userProfile }
    })

    if (alreadyAuthenticated) {
      history.replace(history.location.pathname);
    } else {
      history.replace('/');
    }
  }

  renewSession(history, dispatch) {
    const token = localStorage.getItem('token');

    axios.get(API_URL + "api/users/profile", {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
    })
    .then((res) => {
      this.setSession(res.data.token, res.data.user, history, dispatch);
    })
    .catch((err)=> {
      console.log(err);
      this.logout(history);
    })
  }

  isAuthenticated() {
    if (localStorage.getItem('isLoggedIn') && localStorage.getItem('token')) {
      const token = localStorage.getItem('token');
      const decodedToken = decode(token);
      return decodedToken.exp * 1000 > new Date().getTime();
    }

  }

}

