import axios from 'axios';

const API_URL = process.env.NODE_ENV === "production" ? "https://sergioandguidinha.com/" : "http://localhost:5000/";
const TEST = "https://jsonplaceholder.typicode.com";

const request = {
    get: async (url, options) => {
        try {
            let res = await axios.get(`${API_URL}${url}`, options);
            return res;
        } catch (err) {
            console.error({
                success: false,
                message: "Impossible to retrieve data",
                error: err
            });
        }
    },
    post: async (url, body) => {
        try {
            let res = await axios.post(`${API_URL}${url}`, body);
            return res;
        } catch (err) {
            console.error({
                success: false,
                message: "Impossible to post data, check the body",
                error: err
            });
        }
    }, 
    patch: async (url, body) => {
        try {
            let res = await axios.patch(`${API_URL}${url}`, body);
            return res;
        } catch (err) {
            console.error({
                success: false,
                message: "Impossible to patch data, check the body",
                error: err
            });
        }  
    },
    delete: async (url) => {
        try {
            let res = await axios.delete(`${API_URL}${url}`);
            return res;
        } catch (err) {
            console.error({
                success: false,
                message: "Impossible to send delete request",
                error: err
            });
        }
    }
}

export default {
    API_URL,
    request,
    TEST
}