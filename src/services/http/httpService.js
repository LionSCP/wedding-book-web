import core from '../core/core';

const Pictures = {
    getAll: () => {
        return core.request.get('api/pictures');
    },
    getById: (id) => {
        return core.request.get(`api/pictures/${id}`)
    },
    upload: (formData) => {
        return core.request.post('api/pictures/upload', formData, {
            headers: {
                'content-type': 'multipart/form-data'
            }
        })
    },
    download: (id) => {
        return core.request.get(`api/pictures/download/${id}`, { responseType: 'arraybuffer' })
    },
    update: (id, data) => {
        return core.request.patch(`api/pictures/${id}`, data);
    },
    delete: (id) => {
        return core.request.delete(`api/pictures/${id}`);
    }
}

const Tags = {
    getAllTags: () => {
        return core.request.get('api/pictures/tags');
    }
}

const Guest = {
    getAllGuests: () => {
        return core.request.get('api/users');
    }
}

const Auth = {
    login: (body) => {
        return core.request.post('auth/login', body);
    }
}

const Test = {
    get: () => {
        return core.request.get('/todos');
    }
}

export default {
    Pictures, 
    Auth,
    Guest,
    Tags,
    Test
}