import * as types from '../constants/authActionTypes';
import Auth from '../services/auth/Auth';

const auth = new Auth();

export const authLogin = (body, history) => dispatch => {
  auth.login(body, history, dispatch);
}

export const authCheckAuthentication = (history) => dispatch => {
  if (localStorage.getItem('isLoggedIn') && localStorage.getItem('token') && auth.isAuthenticated()) {
    dispatch({ type: types.CHECK_AUTHENTICATION_SUCCESS});
    auth.renewSession(history, dispatch);
  }
}

export const authLogout = (history) => dispatch => {
  dispatch({
    type: types.LOGOUT
  });
  auth.logout(history);
}

export const handleChangeUsername = value => dispatch => {
  dispatch({
    type: types.HANDLE_USERNAME_CHANGED,
    payload: { username: value }
  });
}

export const handleChangePassword = value => dispatch => {
  dispatch({
    type: types.HANDLE_PASSWORD_CHANGED,
    payload: { password: value }
  })
}