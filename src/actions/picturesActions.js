import * as types from '../constants/picturesActionTypes';
import httpService from '../services/http/httpService';

const API_URL = process.env.NODE_ENV === "production" ? "https://sergioandguidinha.com" : "http://localhost:5000";

export const getAllPictures = () => async dispatch => {
  try {
    dispatch({ type: types.GET_PICTURES_START });
    let pictures = await httpService.Pictures.getAll();
    pictures = Object.values(pictures.data.data).map(picture => {
      let clearPath = picture.path.split(String.fromCharCode(92)).join("/");
      picture.path = `${API_URL}/${clearPath}`;
      return picture
    })
    dispatch({
      type: types.GET_PICTURES_SUCCESS,
      payload: {
        pictures
      }
    });
  } catch (error) {
    dispatch({ type: types.GET_PICTURES_FAIL, payload: { error } });
  }
};

export const getFilteredList = (pictures, keywordsList) => async dispatch => {
  dispatch({ type: types.GET_FILTERED_LIST_START });
  let filteredList = pictures.filter(item => {
    if (item.tags) {
      return item.tags.some(element => {
        return keywordsList[1].includes(element);
      })
    } else return null;
  })

  let list = [];

  for (let keyword of keywordsList[0]) {
    list = pictures.filter((item) => {
      return item.members.some(element => {
        return element.id === keyword.id
      });
    })
  }

  filteredList.push(...list);
  filteredList = [...new Set(filteredList)];

  dispatch({ type: types.GET_FILTERED_LIST_SUCCESS, payload: { filteredList } });
};

export const onOpenModal = (picture) => dispatch => {
  dispatch({ type: types.ON_OPEN_MODAL, payload: { modalContent: picture } });
} 

export const onCloseModal = () => dispatch => {
  dispatch({ type: types.ON_CLOSE_MODAL });
}