import * as types from '../constants/searchActionTypes';

export const addKeyword = keyword => dispatch => {
  dispatch({
    type: types.ADD_KEYWORD,
    payload: { keyword }
  });
}

export const removeKeyword = ( keyword ) => dispatch => {
  dispatch({
    type: types.REMOVE_KEYWORD,
    payload: { keyword }
  });
}

export const addMember = member => dispatch => {
  dispatch({
    type: types.ADD_MEMBER,
    payload: { member }
  });
}

export const removeMember = member => dispatch => {
  dispatch({
    type: types.REMOVE_MEMBER,
    payload: { member }
  })
}

export const handleChangeSearchValue = value => dispatch => {
  dispatch({
    type: types.HANDLE_CHANGE_SEARCH_VALUE,
    payload: { searchValue: value }
  })
}

export const cleanSearchValue = () => dispatch => {
  dispatch({ type: types.CLEAN_SEARCH_VALUE })
}