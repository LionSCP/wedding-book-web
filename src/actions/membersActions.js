import * as types from '../constants/membersActionTypes';
import httpService from '../services/http/httpService';

export const getAllMembers = () => async dispatch => {
  try {
    dispatch({ type: types.GET_ALL_MEMBERS_START });
    let members = await httpService.Guest.getAllGuests();
    dispatch({
      type: types.GET_ALL_MEMBERS_SUCCESS,
      payload: {
        members: members.data.data
      }
    });
  } catch (error) {
    dispatch({type: types.GET_ALL_MEMBERS_FAIL, payload: { error }})
  }
}