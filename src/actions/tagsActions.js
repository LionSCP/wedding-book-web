import * as types from '../constants/tagsActionTypes';
import httpService from '../services/http/httpService';

export const getAllTags = () => async dispatch => {
  try {
    dispatch({ type: types.GET_ALL_TAGS_START });
    let tags = await httpService.Tags.getAllTags();
    dispatch({
      type: types.GET_ALL_TAGS_SUCCESS,
      payload: {
        tags: tags.data.data
      }
    });
  } catch (error) {
    dispatch({ type: types.GET_ALL_TAGS_FAIL, payload: { error }});
  }
}