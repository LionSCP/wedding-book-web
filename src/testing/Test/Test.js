import React from 'react';

/* export function tick() {
  const element = (
    <div>
      <h1>Hello, world !</h1>
      <h2>It is {new Date().toLocaleTimeString()}.</h2>
    </div>
  )
  return element;
}; */

export function CoolComponent({name}) {
  return <p>Youpi so Cool {name}!</p>
};